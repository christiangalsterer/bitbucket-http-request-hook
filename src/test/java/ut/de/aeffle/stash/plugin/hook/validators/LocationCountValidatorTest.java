/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ut.de.aeffle.stash.plugin.hook.validators;

import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scope.Scope;
import com.atlassian.bitbucket.setting.Settings;
import com.atlassian.bitbucket.setting.SettingsValidationErrors;
import com.atlassian.bitbucket.setting.SettingsValidator;
import de.aeffle.stash.plugin.hook.ConfigValidator;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class LocationCountValidatorTest {
    private SettingsValidator configValidator;

    private Settings settings;
    private SettingsValidationErrors errors;
    private Scope scope;

    @Before
    public void beforeTestCreateCleanPlayground() {
        configValidator = new ConfigValidator();

        settings = mock(Settings.class);
        errors = mock(SettingsValidationErrors.class);
        scope = mock(Scope.class);
    }

    private void setLocationCount(String count) {
        when(settings.getString("locationCount", "1")).thenReturn(count);
    }

    private void setUrl(int id, String url) {
        String urlName = ( id > 1 ? "url" + id : "url" );
        when(settings.getString(urlName, "")).thenReturn(url);
    }

    @Test
    public void testValidateWithInvalidCount() {
        //GIVEN
        setLocationCount("-1");

        //WHEN
        configValidator.validate(settings, errors, scope);

        //THEN
        verify(errors, times(1)).addFieldError(eq("locationCount"), anyString());
    }

    @Test
    public void testValidateWithInvalidCount2() {
        //GIVEN
        setLocationCount("11");
        for (int i = 1; i <= 10; i++) {
            setUrl(i, "");
        }

        //WHEN
        configValidator.validate(settings, errors, scope);

        //THEN
        verify(errors, times(1)).addFieldError(eq("locationCount"), anyString());
    }
}
