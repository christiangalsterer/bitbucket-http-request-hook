package de.aeffle.stash.plugin.hook.persistence;


import com.atlassian.bitbucket.repository.Repository;
import de.aeffle.stash.plugin.hook.persistence.ao.AuditLog;

public interface AuditLogService {
    public AuditLog[] getAuditLog();
    public AuditLog[] getAuditLog(Repository repo);
    public AuditLog createAuditLogEntry(Repository repo, String url, Integer responseCode, String responseMessage);
}
